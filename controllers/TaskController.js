// Controllers contain the functions and business logic of our Express JS app
// Meaning all the operations it can perform will be written in this file

const Task = require("../models/Task.js")

// Controller Functions:
module.exports.getAllTasks = () => {
    return Task.find({}).then(result => {
        return result
    })
};

module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})
	// Saves the newly created "newTask" object in the MongoDB database
	// The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
	return newTask.save().then((savedTask, error) => {
		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(error) {
			console.log(error)
			return false
		} else {
			return savedTask //'Task created successfully!'
		}
		
	})
}	

// Function for updating a task
module.exports.updateTask = (taskId,newContent) =>{
    return Task.findById(taskId).then((result,error)=>{
        if(error){
            console.log(error)
            return false
        }
        result.name = newContent.name
        return result.save().then((updatedTask,error)=>{
            if(error){
                console.log(error)
                return false;
            }else{
                return updatedTask
            }
        })
    })
};

// Function for deleting a task
module.exports.deleteTask = (taskId) =>{
    return Task.findByIdAndRemove(taskId).then((deletedTask,error)=>{
        if(error){
            console.log(error)
            return false
        }else{
            return `Successfully deleted ${deletedTask.name}`
        }
    })
};

// Function for Specific
module.exports.specificTask=(id)=>{
    return Task.findById(id).then((foundTask,error)=>{
        if(error){
            return false
        }else{
            return foundTask
        }
    })
};

module.exports.completeTask=(id) =>{
    return Task.findById(id).then((result,err)=>{
        if(err){
            console.log(err)
            return false
        }
        result.status = "complete"
        return result.save().then((updatedTask,err)=>{
            if(err){
                return false;
            }else{
                return updatedTask
            }
        })
    })
}